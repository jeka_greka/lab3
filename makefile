all: scalar_product prime_number

scalar_product: scalar_product.c
	gcc -o scalar_product scalar_product.c

prime_number: prime_number.c
	gcc -o prime_number prime_number.c

clean:
	rm -f scalar_product prime_number
	
