#include <stdio.h>


const int array1[] = {1,2,3};
const int array2[] = {4,5,6};

int scalar_product(const int* array1, const int* array2, size_t count){
	int scalar_product = 0;
	size_t i;
	for(i = 0; i<count; i++)  {
		scalar_product += array1[i]*array2[i];
	}
	return scalar_product;
}


int main(void){
	printf(
		"The scalar product of two vectors: %d\n",
		scalar_product(array1, array2, sizeof(array1)/sizeof(int))
		);

}
