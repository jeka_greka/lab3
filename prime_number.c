#include <stdio.h>


int is_prime(unsigned long number){
	unsigned long i;
	if(number <= 1) return 0;	
	for(i = 2; i*i < number; i++){
		if(number % i == 0) return 0;		
	}
	return 1;

}


int main(void){
	unsigned long number;
	printf("Next your number:\n");
	scanf("%lu", &number);
	if(is_prime(number)) printf("This is a prime number.\n");
	else printf("This isn't a prime number.\n");
}
